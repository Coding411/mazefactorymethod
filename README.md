# MazeFactoryMethod
Factory Pattern from GoF Design Patterns book.

## Definition
Defines an interface for creating an object, but let subclasses decide which class to instantiate.

## Applicability
1) When a class cant anticipate the class of objects it must create.
2) A class wants it's subclasses to specify the objects it creates.

## Participants
1) Product: Defines the interface of objects the factory creates.
2) ConcreteProduct: Implements the Product interface.
3) Creator/Factory: Declares the factory methods that return an object of type Product. May also define a default implementation of factory methods.
4) ConcreteCreator/ConcreteFactory: Overrides the factory method to return an instance of a ConcreteProduct.

## Implementation
- Abstract Creator classes can either provide a default implementation or not.
- Parameterized factory methods lets the factory create multiple kinds of products. Parameter identifies the kind of objects to create. All objects created share the Product interface.