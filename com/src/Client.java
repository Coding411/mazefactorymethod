import mazeFactory.MazeFactory;
import mazeFactory.SimpleMazeFactory;

public class Client {

    public static void main(String[] args) {
        MazeFactory factory = new SimpleMazeFactory();

        // Un-parameterized factory methods
        factory.createMagicalMaze();
        factory.createMazeWithCaves();

        // Parameterized factory method
        factory.createMaze("magic");
    }
}
