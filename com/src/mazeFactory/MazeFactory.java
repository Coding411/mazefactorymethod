package mazeFactory;

import mazeFactory.maze.Maze;

public interface MazeFactory {

    // Factory methods

    // Creates a mazeFactory.maze.Maze with enchanted rooms and doors with spells
    Maze createMagicalMaze();

    // Creates a mazeFactory.maze.Maze with caves and trees
    Maze createMazeWithCaves();

    // Creates a magical or enchanted maze based on argument.
    Maze createMaze(final String mazeType);
}
