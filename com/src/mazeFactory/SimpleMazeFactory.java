package mazeFactory;

import mazeFactory.maze.EnchantedMaze;
import mazeFactory.maze.HobbitMaze;
import mazeFactory.maze.Maze;

public class SimpleMazeFactory implements MazeFactory{

    @Override
    public Maze createMaze(final String mazeType) {
        if("magic".equalsIgnoreCase(mazeType)) {
            return createMagicalMaze();
        } else {
            return createMazeWithCaves();
        }
    }

    @Override
    public Maze createMagicalMaze() {
        return new EnchantedMaze();
    }

    @Override
    public Maze createMazeWithCaves() {
        return new HobbitMaze();
    }
}
